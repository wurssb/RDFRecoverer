#!/bin/bash
#============================================================================
#title          :SAPP Module
#description    :Test script for Galaxy / SAPP
#author         :Jasper Koehorst
#date           :2016
#version        :0.0.1
#============================================================================

planemo test --test_data . $*

