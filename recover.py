# pip3.4 install networkx
from networkx.readwrite import json_graph
from shutil import copyfile
from subprocess import Popen, PIPE, STDOUT
import os
import re
import sys
import networkx as nx
from SPARQLWrapper import SPARQLWrapper, JSON


### LOADING into blazegraph
# http://localhost:9999/blazegraph/namespace/kb/sparql
#ENDPOINT = "http://ssb5.wurnet.nl:9999/blazegraph/namespace/hadoop/sparql"
#print("Want to use?: "+ENDPOINT)

global SAVEFILE
RDFFILE = ""
SAVEFILE = sys.argv[sys.argv.index("-output") + 1]
if "-input" in sys.argv:
    RDFFILE = sys.argv[sys.argv.index("-input") + 1]
ENDPOINT = sys.argv[sys.argv.index("-endpoint") + 1]

REPLACEMENTS = {
	"gbol:":"http://gbol.life/0.1/",
    "ssb:":"http://csb.wur.nl/genome/",
	"biopax:":"http://www.biopax.org/release/bp-level3.owl#",
	"rdf:":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",
	"owl:":"http://www.w3.org/2002/07/owl#",
	"rdfs:":"http://www.w3.org/2000/01/rdf-schema#",
    "jerm:":"http://jermontology.org/ontology/JERMOntology",
    "unlock:":"http://m-unlock.nl/ontology/",
    "ppeo:":"http://purl.org/ppeo/PPEO.owl#observation_unit",
    "prov":"http://www.w3.org/ns/prov",
    "foaf:":"http://xmlns.com/foaf/0.1/",
    "ssb:":"http://semantics.systemsbiology.nl/0.1/",
    "ro:":"http://purl.org/wf4ever/ro#",
    "oa:":"http://www.openarchives.org/ore/terms/",
    "wf4ever:":"http://purl.org/wf4ever/wf4ever",
    "wfdesc:":"http://purl.org/wf4ever/wfdesc",
    "wf:":"http://purl.org/wf4ever/wfprov",
    "provs":"https://w3id.org/cwl/prov#"
}

IGNORE = [
    "http://purl.org/wf4ever/ro#",
    "http://www.openarchives.org/ore/terms/",
    "http://purl.org/wf4ever/wf4ever",
    "http://purl.org/wf4ever/wfdesc",
    "http://purl.org/wf4ever/wfprov",
    "http://www.w3.org/ns/prov",
    "https://w3id.org/cwl/prov#"
]

global ROOT
ROOT = os.path.realpath(__file__).rsplit("/",1)[0]

if RDFFILE != "":
    for file in RDFFILE.split(","):
        command = "curl -# -X POST -H 'Content-Type:application/x-turtle' --data-binary '@"+file+"' http://localhost:9999/blazegraph/namespace/kb/sparql"
        print(command)
        os.system(command)
#############

global G
G = nx.DiGraph()

PREFIX = "prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#> "

SPARQL = SPARQLWrapper(ENDPOINT)
SPARQL.setReturnFormat(JSON)


def main():
        getClasses()


def getClasses():
        QUERY = " SELECT DISTINCT ?type WHERE { ?class a ?type . }"
        SPARQL.setQuery(QUERY)

        results = SPARQL.query().convert()

        for result in results['results']['bindings']:
            type = result['type']['value']
            getPredicates(type)
            save()


def getPredicates(classURI):
    QUERY = PREFIX + " SELECT DISTINCT ?predicate WHERE { VALUES ?type { <"+classURI+">} ?class a ?type . ?class ?predicate ?object . }"

    SPARQL.setQuery(QUERY)

    results = SPARQL.query().convert()

    for result in results['results']['bindings']:
        predicate = result['predicate']['value']
        for element in IGNORE:
            if element in predicate:
                print("SKipping " + predicate)
                predicate = None
                break

        if predicate == None: continue

        G.add_node(classURI, graphics={"fill":"#ff0000","w":100})

        print("Node: "+classURI+" added...")
        getClassConnections(classURI,predicate)

        G.add_node(predicate, graphics={"type":"ellipse","w":100})
        print(classURI, predicate)
        G.add_edge(classURI,predicate)


def getClassConnections(classURI, predicateURI):
        QUERY = PREFIX + """
            SELECT DISTINCT ?class
            WHERE {
            VALUES ?type { <"""+classURI+""">}
            VALUES ?predicate { <"""+predicateURI+""">}
            ?subject a ?type .
            ?subject ?predicate ?object .
            ?object a ?class .
            }"""
        SPARQL.setQuery(QUERY)

        results = SPARQL.query().convert()

        for result in results['results']['bindings']:
            class2 = result['class']['value']
            #Changing predicateURI to <CLASS>pred<CLASS>
            G.add_edge("<"+classURI+">"+predicateURI+"<"+class2+">",class2)
            G.add_edge(classURI,"<"+classURI+">"+predicateURI+"<"+class2+">")

def save():
        print("saving...")
        nx.write_gml(G, SAVEFILE)

        content = str(open(SAVEFILE).read())
        for key in REPLACEMENTS:
            content = content.replace(REPLACEMENTS[key], key)
        open(SAVEFILE, "w").write(content)


if __name__ == '__main__':
        main()

