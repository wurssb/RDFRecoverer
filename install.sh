#!/bin/bash
#============================================================================
#title          :RDFRecoverer
#description    :RDF recoverer for HDT files
#author         :Jasper Koehorst
#date           :2016
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

if [ "$1" == "test" ]; then
	pip3 install SPARQLWrapper
	pip3 install networkx
	pip3 install rdfextras
fi
